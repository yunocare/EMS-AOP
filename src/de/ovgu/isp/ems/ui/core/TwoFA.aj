package de.ovgu.isp.ems.ui.core;

public aspect TwoFA {
	
	/**
	 * @return SQL login query
	 */
	String around(): execution(String getLoginQuery()){
		return "select id,username,password from Users Where (username =? and password =?)";
	}

}
