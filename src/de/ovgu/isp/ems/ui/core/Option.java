package de.ovgu.isp.ems.ui.core;

import java.awt.event.ActionListener;

public class Option {
	
	String optionName;
	ActionListener optionListener;
	
	public Option(String name, ActionListener openDialog){
		this.optionName = name;
		this.optionListener = openDialog;
	}
	
	public String getOptionName(){
		return optionName;
	}

}
