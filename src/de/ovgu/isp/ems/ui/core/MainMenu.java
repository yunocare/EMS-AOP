package de.ovgu.isp.ems.ui.core; 
import java.awt.BorderLayout;
import java.awt.Dimension; 
import java.awt.GridBagLayout;
import java.awt.Toolkit; 

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.itextpdf.text.pdf.PdfWriter; 
import java.awt.Color; 
import java.awt.event.ActionEvent; 
import java.awt.event.MouseListener; 
import com.itextpdf.text.Document; 
import com.itextpdf.text.FontFactory; 
import com.itextpdf.text.Paragraph; 
import com.itextpdf.text.pdf.PdfPCell; 
import com.itextpdf.text.pdf.PdfPTable; 
import java.awt.Font; 
import java.io.File; 
import java.io.FileOutputStream; 
import java.sql.Connection; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet; 

import javax.swing.ImageIcon; 
import javax.swing.JButton; 
import javax.swing.JFileChooser; 
import javax.swing.JFrame; 
import javax.swing.JLabel; 
import javax.swing.JList; 
import javax.swing.JOptionPane; 
import javax.swing.JMenu; 
import javax.swing.JMenuItem; 
import javax.swing.JTextField; 
import javax.swing.JPanel; 
import javax.swing.JMenuBar; 
import javax.swing.JScrollPane; 
import java.util.Date; 
import java.util.ResourceBundle; 
import de.ovgu.isp.ems.core.Localization; 
import de.ovgu.isp.ems.core.Theme; 
import de.ovgu.isp.ems.db.db; 
import de.ovgu.isp.ems.db.Emp; 
import de.ovgu.isp.ems.ui.users; 
import de.ovgu.isp.ems.ui.Allowance; 
import de.ovgu.isp.ems.ui.ReportGenerator; 
import de.ovgu.isp.ems.ui.employeeDeductions; 
import de.ovgu.isp.ems.ui.searchEmpSalarySlip; 
import de.ovgu.isp.ems.logging.ApplicationLogger; 
import org.apache.logging.log4j.Logger; 

import de.ovgu.isp.ems.ui.searchEmployee; 

import de.ovgu.isp.ems.ui.updateSalary; 
import de.ovgu.isp.ems.ui.Audit_details; 
import de.ovgu.isp.ems.ui.addEmployee; 

public   class  MainMenu  extends javax.swing.JFrame {
	

	/*
	 * Main Menu JFrame Configuration:
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	
	int window_height = (int)(screenSize.getHeight());

	
	int window_width = (int)(screenSize.getWidth())/2;

	
	
	/*
	 * ButtonMenuUI Configuration:
	 */
	int _buttonhight = 180;

	
	int _buttonwidth = 300;

	
	int _buttonY = (window_width / 2 - _buttonwidth);

	 // center
	int _buttonYoffset = 0;

	
	int _buttonX = 300;

	
	private static final long serialVersionUID = 1L;

	

	Connection conn = null;

	
	ResultSet rs = null;

	
	PreparedStatement pst = null;

	
	// int panel_height;
	// int panel width;

	/**
	 * Creates new JFrame MainMenu
	 */
	public MainMenu() {
		initComponents();
		conn = db.java_db();

		// TODO: set Username to Label:
		txt_emp.setText(String.valueOf(Emp.empId).toString());

	}

	
	
	

	



	

		

	

	
	private void initComponents() {
		// JFrame Window Configuration:
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(true);
		setPreferredSize(new Dimension(500, 500));

		
		// Panel:
		jPanel2 = new JPanel();

		// Menu:
		btn_menu = new JMenu();
		jMenu1 = new JMenu();
		jMenu3 = new JMenu();
		jMenu4 = new JMenu();
		jMenu5 = new JMenu();
		jMenu7 = new JMenu();

		// Menu Bar:
		jMenuBar1 = new JMenuBar();
		jMenuBar2 = new JMenuBar();

		jMenuItem1 = new JMenuItem();
		jMenuItem2 = new JMenuItem();
		jMenuItem3 = new JMenuItem();
		jMenuItem4 = new JMenuItem();
		jMenuItem6 = new JMenuItem();
		jMenuItem7 = new JMenuItem();
		jMenuItem8 = new JMenuItem();
		jMenuItem13 = new JMenuItem();

		// Label:
		jLabel1 = new JLabel();
		txt_emp = new JLabel();
		jLabel2 = new JLabel();
		jLabel3 = new JLabel();

		// Text fields:
		jTextField1 = new JTextField();

		// Scroll Pane:
		jScrollPane1 = new JScrollPane();

		
		
		// Initialize EMPLOYEE Menu (top left in menu bar)
		jMenu1.setText(messages.getString("employee"));

		// Add Employee Registration entry to EMPLOYEE menu
		jMenuItem1.setText(messages.getString("emp_registration"));
		jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				add_employeeActionPerformed(evt);
			}
		});
		// Use JMenuBar
		jMenu1.add(jMenuItem1);
		jMenuBar1.add(jMenu1);
		
	
		// List:

		// ----------------------------------------------------Theme-----------------------------------------------------------------//

		/*
		 * Set theme font, color and background
		 */
		// TODO: check if new object Theme is necessary
		Theme t = new Theme();
		int _fontsize = t.getSize();
		_icon = new ImageIcon(getClass().getResource(t.getThemeBackground()));
		_fontColor = t.getColor();

		// ----------------------------------------------------Theme-----------------------------------------------------------------//

		jMenu3.setText(messages.getString("file"));
		jMenuBar2.add(jMenu3);

		jMenu4.setText(messages.getString("edit"));
		jMenuBar2.add(jMenu4);
		jPanel2.setLayout(null);

		// ----------------------------------------------------Buttons-----------------------------------------------------------------//

		jButton2.setText(messages.getString("allowance"));
		jButton3.setText(messages.getString("deduction"));
		jButton4.setText(messages.getString("search"));
		jButton6.setText(messages.getString("logout"));
		jButton7.setText(messages.getString("payment"));
		jButton8.setText(messages.getString("report_generator"));

		// JButton6: Logout
		jButton6.setBackground(new java.awt.Color(255, 255, 255));
		jButton6.setFont(_font);
		jButton6.setForeground(_fontColor);
		jButton6.setIcon(new ImageIcon(getClass().getResource("/Images/logout.png"))); // NOI18N

		int logoutButtonLength = 150;
		jPanel2.add(jButton6);
		jButton6.setBounds(window_width - logoutButtonLength - 10, 10, 150, 50);

		// JButton7: PAYMENT
		jButton7.setFont(_font); // NOI18N
		jButton7.setForeground(_fontColor);
		jButton7.setIcon(new ImageIcon(getClass().getResource("/Images/Payment.png"))); // NOI18N
		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonActionSearchSalarySlip(evt);
			}
		});
		jPanel2.add(jButton7);
		jButton7.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX + (_buttonhight * 3), _buttonwidth,
				_buttonhight);

		// JButton 8: REPORT GENERATOR
		jButton8.setFont(_font); // NOI18N
		jButton8.setForeground(_fontColor);
		jButton8.setIcon(new ImageIcon(getClass().getResource("/Images/Payment.png"))); // NOI18N
		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonActionReportGenerator(evt);
			}

		});
		jPanel2.add(jButton8);
		jButton8.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX, _buttonwidth, _buttonhight);


		// JButton3: DEDUCTION
		jButton3.setFont(_font); // NOI18N
		jButton3.setForeground(_fontColor);

		jButton3.setIcon(new ImageIcon(getClass().getResource("/Images/Deduction.png"))); // NOI18N
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonActionEmployeeDeduction(evt);
			}
		});
		jPanel2.add(jButton3);
		jButton3.setBounds(_buttonY, _buttonX + (_buttonhight * 3), _buttonwidth, _buttonhight);
		// JButton2: ALLOWANCE
		jButton2.setFont(_font); // NOI18N
		jButton2.setForeground(_fontColor);
		jButton2.setIcon(new ImageIcon(getClass().getResource("/Images/Allowance.png"))); // NOI18N
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonActionAllowance(evt);
			}
		});
		jPanel2.add(jButton2);
		jButton2.setBounds(_buttonY, _buttonX + (_buttonhight * 2), _buttonwidth, _buttonhight);

		// JButton4 : SEARCH
		jButton4.setFont(_font); // NOI18N
		jButton4.setForeground(_fontColor);

		jButton4.setIcon(new ImageIcon(getClass().getResource("/Images/Search.png"))); // NOI18N
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				openSearchEmployeeDialog(evt);
			}
		});
		jPanel2.add(jButton4);
		jButton4.setBounds(_buttonY + _buttonwidth + _buttonYoffset, _buttonX + _buttonhight, _buttonwidth,
				_buttonhight);

		// --------------------------------------------------</Buttons>----------------------------------------------------------------//

		// ------------------------------------------------------Labels-----------------------------------------------------------------//
		// ------Label: Logged in as------//
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText(messages.getString("logged_in_as") + " :");
		jPanel2.add(jLabel1);
		jLabel1.setBounds(10, window_height - 100, 180, 14);

		txt_emp.setForeground(new java.awt.Color(255, 255, 255));
		// TODO: setText(username) instead of Employee ID
		txt_emp.setText(messages.getString("employee_id"));
		jPanel2.add(txt_emp);
		txt_emp.setBounds(130, window_height - 100, 80, 14);

		jLabel2.setIcon(_icon);
		jPanel2.add(jLabel2);
		jLabel2.setBounds(0, 0, window_width + 20, window_height); // TODO: adjust background image size
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setUndecorated(true);
		setVisible(true);	
		// Pack JFrame
		pack();
		// ----------------------------------------------------</Labels>----------------------------------------------------------------//

		// -------------------------------------------------------Menu------------------------------------------------------------------//
		// TODO ...
		jTextField1.setText("jTextField1");

		// Initialize REPORTS Menu (second left in menu bar)
		jMenu5.setText(messages.getString("reports"));
		// Add Employee Reports to Menu
		jMenuItem6.setText(messages.getString("emp_reports"));
		jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				itemActionGenerateEmpReport(evt);
			}
		});
		jMenu5.add(jMenuItem6);

		jMenuItem7.setText(messages.getString("emp_total_allowanceRP"));
		jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				itemActionGenerateAllowanceReport(evt);
			}
		});
		jMenu5.add(jMenuItem7);

		jMenuItem2.setText(messages.getString("emp_total_deduction"));
		jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				itemActionGenerateEmpDeductionsReport(evt);
			}
		});
		jMenu5.add(jMenuItem2);

		jMenuBar1.add(jMenu5);

		btn_menu.setText(messages.getString("audit"));

		jMenuItem13.setText(messages.getString("add_user"));
		jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				buttonActionUsers(evt);
			}
		});
		btn_menu.add(jMenuItem13);

		jMenuBar1.add(btn_menu);

		jMenu7.setText(messages.getString("about"));
		jMenuItem3.setText("Version 1.0");
		jMenu7.add(jMenuItem3);
		jMenuItem4.setText("Support");
		jMenu7.add(jMenuItem4);
		jMenuItem8.setText("About EMS project");
		jMenu7.add(jMenuItem8);
		jMenuBar1.add(jMenu7);

		setJMenuBar(jMenuBar1);
		pack();
		setVisible(true);

		
		
	}


	// ------------------------------------------------------------------------------------------------------------------------------------------//

	// ---------------------------------------------------------ACTION_HANDLER-------------------------------------------------------------------//

	/**
	 * Button: Users
	 * 
	 * @param evt
	 */
	private void buttonActionUsers(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonActionUsers
		users x = new users();
		x.setVisible(true);
	}

	

	/**
	 * Button: Allowance
	 * 
	 * @param evt
	 */
	private void buttonActionAllowance(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonActionAllowance
		Allowance x = new Allowance();
		x.setVisible(true);
	}

	// GEN-LAST:event_buttonActionAllowance

	/**
	 * Button: Report Generator
	 * 
	 * @param evt
	 */
	private void buttonActionReportGenerator(java.awt.event.ActionEvent evt) {
		// TODO: implement logic
		System.out.println("Report Generator clicked");
		ReportGenerator _rg = new ReportGenerator();
		_rg.setVisible(true);
	}

	

	/**
	 * Button: Logout
	 * 
	 * @param evt
	 */

	// GEN-LAST:event_buttonActionLogout

	/**
	 * 
	 * @param evt
	 */
	private void itemActionGenerateEmpReport(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateEmpReport
		// Dialog:
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employees Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);

		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			try {

				// #if ApplicationLogging
				// @ log.info("Preparing SQL statement to SELECT * FROM TABLE" +
				// @ "STAFF_INFORMATION");
				// #endif

				String sql = "select * from Staff_information";

				pst = conn.prepareStatement(sql);
				rs = pst.executeQuery();

				Document myDocument = new Document();
				PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath));
				PdfPTable table = new PdfPTable(13);
				myDocument.open();

				float[] columnWidths = new float[] { 3, 8, 7, 5, 5, 9, 8, 9, 6, 6, 6, 8, 8 };
				table.setWidths(columnWidths);

				table.setWidthPercentage(100); // set table width to 100%

				myDocument.add(new Paragraph(messages.getString("emp_list"),
						FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD)));
				myDocument.add(new Paragraph(new Date().toString()));
				myDocument.add(new Paragraph(
						"-------------------------------------------------------------------------------------------"));
				table.addCell(
						new PdfPCell(new Paragraph("ID", FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("firstname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("surname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("date_of_birth"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("email"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("tel"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("address"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("department"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("gender"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("salary"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("status"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("date_hired"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("job_titel"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.BOLD))));

				while (rs.next()) {

					table.addCell(new PdfPCell(new Paragraph(rs.getString(1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(2),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(3),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(4),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(5),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(6),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(7),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(8),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(10),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(11),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(16),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(17),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(18),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 8, Font.PLAIN))));

				}

				// #if ApplicationLogging
				// @ log.info("Add table to pdf document.");
				// #endif
				myDocument.add(table);
				myDocument.add(new Paragraph(
						"--------------------------------------------------------------------------------------------"));
				myDocument.close();
				// #if ApplicationLogging
				// @ log.info("closing pdf document");
				// @ log.info("successfully finished pdf report generation.");
				// #endif
				JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);

				}
			}
		}

	}

	// GEN-LAST:event_itemActionGenerateEmpReport

	/**
	 * 
	 * @param evt
	 */
	private void itemActionGenerateAllowanceReport(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateAllowanceReport

		// TODO put method into class Allowance
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employee Allowance Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			try {

				// #if ApplicationLogging
				// @ log.info("Preparing SQL statement to SELECT * FROM TABLE ALLOWANCE");
				// #endif

				String sql = "select * from Allowance";

				pst = conn.prepareStatement(sql);
				rs = pst.executeQuery();

				Document myDocument = new Document();
				PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath));
				PdfPTable table = new PdfPTable(11);
				myDocument.open();

				float[] columnWidths = new float[] { 3, 7, 7, 5, 5, 9, 6, 5, 8, 8, 8 };
				table.setWidths(columnWidths);

				table.setWidthPercentage(100); // set table width to 100%

				myDocument.add(new Paragraph(messages.getString("emp_allowance_list"),
						FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD)));
				myDocument.add(new Paragraph(new Date().toString()));
				myDocument.add(new Paragraph(
						"-------------------------------------------------------------------------------------------"));
				table.addCell(
						new PdfPCell(new Paragraph("ID", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("overtime"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("medical"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("bonus"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("other"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("employee_id"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("salary"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("rate"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("allowance"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("firstname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("surname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));

				while (rs.next()) {

					table.addCell(new PdfPCell(new Paragraph(rs.getString(1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(2),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(3),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(4),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(5),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(6),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(7),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(8),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(9),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(10),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(11),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));

				}

				myDocument.add(table);
				myDocument.add(new Paragraph(
						"--------------------------------------------------------------------------------------------"));
				myDocument.close();
				// #if ApplicationLogging
				// @ log.info("closing pdf document");
				// @ log.info("successfully finished pdf report generation.");
				// #endif

				JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);

				}
			}
		}

	}

	// GEN-LAST:event_itemActionGenerateAllowanceReport

	/**
	 * Button: Employee Deduction
	 * 
	 * @param evt
	 */
	private void buttonActionEmployeeDeduction(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonActionEmployeeDeduction

		employeeDeductions x = new employeeDeductions();
		x.setVisible(true);

	}

	// GEN-LAST:event_buttonActionEmployeeDeduction

	/**
	 * Button: Search Emp Salary Slip
	 * 
	 * @param evt
	 */
	private void buttonActionSearchSalarySlip(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_buttonActionSearchSalarySlip
		// TODO add your handling code here:
		System.out.println("Button Search Emp Salary Slip clicked.");

		searchEmpSalarySlip x = new searchEmpSalarySlip();
		x.setVisible(true);

	}

	// GEN-LAST:event_buttonActionSearchSalarySlip

	/**
	 * Item:
	 * 
	 * @param evt
	 */
	private void itemActionGenerateEmpDeductionsReport(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_itemActionGenerateEmpDeductionsReport

		// #if ApplicationLogging
		// @ log.info("Starting PDF export for employee deduction report");
		// #endif
		JFileChooser dialog = new JFileChooser();
		dialog.setSelectedFile(new File("Employee Deduction Report.pdf"));
		int dialogResult = dialog.showSaveDialog(null);
		if (dialogResult == JFileChooser.APPROVE_OPTION) {
			String filePath = dialog.getSelectedFile().getPath();

			try {
				// #if ApplicationLogging
				// @ log.info("Preparing SQL statement to SELECT * FROM TABLE DEDUCTIONS");
				// #endif

				String sql = "select * from Deductions";
				pst = conn.prepareStatement(sql);
				rs = pst.executeQuery();

				Document myDocument = new Document();
				PdfWriter myWriter = PdfWriter.getInstance(myDocument, new FileOutputStream(filePath));
				PdfPTable table = new PdfPTable(8);
				myDocument.open();

				// #if ApplicationLogging
				// @ log.info("pdf export to document " + myDocument.getId() + " started.");
				// #endif

				float[] columnWidths = new float[] { 3, 7, 7, 5, 5, 9, 6, 5 };
				table.setWidths(columnWidths);

				table.setWidthPercentage(100); // set table width to 100%

				myDocument.add(new Paragraph(messages.getString("emp_deduction_list"),
						FontFactory.getFont(FontFactory.TIMES_BOLD, 20, Font.BOLD)));
				myDocument.add(new Paragraph(new Date().toString()));
				myDocument.add(new Paragraph(
						"-------------------------------------------------------------------------------------------"));
				table.addCell(
						new PdfPCell(new Paragraph("ID", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("firstname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("surname"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("salary"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("deduction_amount"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("deduction_reason"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("employee_id"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));
				table.addCell(new PdfPCell(new Paragraph(messages.getString("created_by"),
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.BOLD))));

				while (rs.next()) {
					table.addCell(new PdfPCell(new Paragraph(rs.getString(1),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(2),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(3),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(4),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(5),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(6),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(7),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));
					table.addCell(new PdfPCell(new Paragraph(rs.getString(8),
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 9, Font.PLAIN))));

				}

				myDocument.add(table);
				// #if ApplicationLogging
				// @ log.info("table result added to pdf document.");
				// #endif

				myDocument.add(new Paragraph(
						"--------------------------------------------------------------------------------------------"));
				myDocument.close();

				// #if ApplicationLogging
				// @ log.info("closing pdf document");
				// @ log.info("successfully finished pdf report generation.");
				// #endif

				JOptionPane.showMessageDialog(null, messages.getString("report_sucessful"));

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {

				try {
					rs.close();
					pst.close();

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e);

				}
			}
		}

	}

	// GEN-LAST:event_itemActionGenerateEmpDeductionsReport

	/**
	 * Base - Main Menu
	 * 
	 * @param args
	 *            no runtime parameter
	 */
	public static void main(String args[]) {

	
				MainMenu f = new MainMenu();
	}

	

	// #if ApplicationLogging
	// @ Logger log = new ApplicationLogger().getApplicationLogger();
	// #endif
	ResourceBundle messages = new Localization().getBundle();

	
	private Font _font;

	 // theme font
	private Color _fontColor;

	 // Text color
	private ImageIcon _icon;

	 // background wallpaper
	// Store all Buttons in a separated array:
	private javax.swing.JButton[] buttonsArray;

	
	// Variables declaration - do not modify//GEN-BEGIN:variables

	JButton jButton2 = new JButton();

	
	JButton jButton3 = new JButton();

	
	JButton jButton4 = new JButton();

	
	JButton jButton6 = new JButton();

	
	JButton jButton7 = new JButton();

	
	JButton jButton8 = new JButton();

	
	// Buttons:
	private javax.swing.JMenu btn_menu;

	

	private javax.swing.JMenu jMenu1;

	
	private javax.swing.JMenu jMenu3;

	
	private javax.swing.JMenu jMenu4;

	
	private javax.swing.JMenu jMenu5;

	
	private javax.swing.JMenu jMenu7;

	
	// Menu Bar
	private javax.swing.JMenuBar jMenuBar1;

	
	private javax.swing.JMenuBar jMenuBar2;

	
	// Menu Items
	private javax.swing.JMenuItem jMenuItem1;

	
	private javax.swing.JMenuItem jMenuItem13;

	
	private javax.swing.JMenuItem jMenuItem2;

	
	private javax.swing.JMenuItem jMenuItem3;

	
	private javax.swing.JMenuItem jMenuItem4;

	
	private javax.swing.JMenuItem jMenuItem6;

	
	private javax.swing.JMenuItem jMenuItem7;

	
	private javax.swing.JMenuItem jMenuItem8;

	
	private JPanel jPanel2  = new JPanel();

	
	// Scroll Pane:
	private javax.swing.JScrollPane jScrollPane1;

	
	// Text Field:
	private javax.swing.JTextField jTextField1;

	
	// Label:
	private javax.swing.JLabel txt_emp;

	
	private javax.swing.JLabel jLabel1;

	
	private javax.swing.JLabel jLabel2;

	
	private javax.swing.JLabel jLabel3;
	
	

	
	
	/**
	 * this methods opens a new JFrame dialog
	 * Main Menu Link: Search Employee
	 * 
	 * @param evt
	 */
	private void openSearchEmployeeDialog(ActionEvent evt) {// GEN-FIRST:event_openSearchEmployeeDialog
		searchEmployee e = new searchEmployee();
		e.setVisible(true);
	}

	
	JButton jButton5 = new JButton();

	

	/**
	 * method opens an update salary JFrame 
	 * 
	 * @param evt
	 */
	private void openUpdateSalaryDialog(ActionEvent evt) {
		updateSalary x = new updateSalary();
		x.setVisible(true);
	}

	

	private JMenuItem jMenuItem5 = new JMenuItem();

	
	JButton jButton9 = new JButton();

	

	/**
	 * When method is called, Audit Details JFrame will be opened
	 * 
	 * @param evt
	 */
	private void openAuditDialog(ActionEvent evt) {// GEN-FIRST:event_openAuditDialog
		Audit_details x = new Audit_details();
		x.setVisible(true);

	}

	
	JButton jButton1 = new JButton();

	



	/**
	 * Button:
	 * 
	 * @param evt
	 */
	private void add_employeeActionPerformed(ActionEvent evt) {// GEN-FIRST:event_add_employeeActionPerformed
		if (evt == null) {
			System.out.println("Click from list menu");
			JFrame f = new addEmployee();
			// TODO: open jframe inside jpanel - outstanding!
		} else {
			addEmployee x = new addEmployee();
			x.setVisible(true);
		}

	}


}
