package de.ovgu.isp.ems.ui; 

import java.awt.Dimension; 
import java.awt.Toolkit; 
import java.awt.Window;
import java.sql.Connection; 
import java.sql.PreparedStatement; 
import java.sql.ResultSet; 
import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.util.Date; 
import java.util.GregorianCalendar; 
import java.util.ResourceBundle; 
import net.proteanit.sql.DbUtils; 

import javax.swing.BoxLayout;
import javax.swing.JOptionPane; 
import de.ovgu.isp.ems.core.Localization; 
import de.ovgu.isp.ems.db.db; 
import de.ovgu.isp.ems.db.Emp; 
import de.ovgu.isp.ems.logging.ApplicationLogger; 
import org.apache.logging.log4j.Logger; 


public  class  Allowance  extends javax.swing.JFrame {
	
    private static final long serialVersionUID = 1L;

	

    
	Connection conn = null;

	
	ResultSet rs = null;

	
	PreparedStatement pst = null;

	
	ResourceBundle messages = new Localization().getBundle();

	

	/**
	 * Creates new form Allowance
	 */
	public Allowance() {
		initComponents();
		conn = db.java_db();
		Update_table();
		Toolkit toolkit = getToolkit();
		Dimension size = toolkit.getScreenSize();
		setLocation(size.width / 2 - getWidth() / 2, size.height / 2 - getHeight() / 2);

		txt_emp.setText(String.valueOf(Emp.empId).toString());

	}

	

	private void Update_table() {
		try {
			String sql = "select * from allowance";
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			table_allowance.setModel(DbUtils.resultSetToTableModel(rs));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {

			try {
				rs.close();
				pst.close();

			} catch (Exception e) {

			}
		}
	}

	

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		txt_empid = new javax.swing.JTextField();
		txt_dob = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel12 = new javax.swing.JLabel();
		txt_salary = new javax.swing.JTextField();
		txt_dept = new javax.swing.JTextField();
		txt_surname = new javax.swing.JTextField();
		jLabel9 = new javax.swing.JLabel();
		txt_firstname = new javax.swing.JTextField();
		jPanel2 = new javax.swing.JPanel();
		txt_med = new javax.swing.JTextField();
		txt_bonus = new javax.swing.JTextField();
		txt_other = new javax.swing.JTextField();
		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		jLabel8 = new javax.swing.JLabel();
		jLabel13 = new javax.swing.JLabel();
		txt_hw = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();
		txt_total_overtime = new javax.swing.JTextField();
		txt_per = new javax.swing.JTextField();
		jLabel15 = new javax.swing.JLabel();
		jLabel11 = new javax.swing.JLabel();
		jButton2 = new javax.swing.JButton();
		jPanel4 = new javax.swing.JPanel();
		jLabel14 = new javax.swing.JLabel();
		txt_search = new javax.swing.JTextField();
		txt_cal = new javax.swing.JButton();
		jButton1 = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		table_allowance = new javax.swing.JTable();
		jLabel10 = new javax.swing.JLabel();
		lbl_total = new javax.swing.JLabel();
		txt_emp = new javax.swing.JLabel();
		jLabel16 = new javax.swing.JLabel();
		jMenuBar1 = new javax.swing.JMenuBar();

		//Allowance JFrame Config:
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
		
		
		//not editable JText:
		txt_empid.setEditable(false);
		txt_dob.setEditable(false);
		txt_dept.setEditable(false);
		txt_surname.setEditable(false);
		txt_firstname.setEditable(false);
		txt_total_overtime.setEditable(false);
		txt_per.setEditable(false);
		txt_salary.setEditable(false);
		

		//set Text to Labels:
		jLabel3.setText(messages.getString("date_of_birth")+" :");
		jLabel2.setText(messages.getString("surname")+" :");
		jLabel1.setText(messages.getString("firstname")+" :");
		jLabel5.setText(messages.getString("employee_id")+ " :");
		jLabel9.setText(messages.getString("department")+" :");
		jLabel12.setText(messages.getString("basic_salary")+ " :");
		jLabel4.setText(messages.getString("overtime")+" :");
		jLabel15.setText(messages.getString("total_overtime")+":");
		jLabel11.setText(messages.getString("rate_per_hour")+" :");
		jLabel13.setText(messages.getString("enter_amounts"));
		jLabel6.setText(messages.getString("medical")+" :");
		jLabel7.setText(messages.getString("bonus")+" :");
		jLabel8.setText(messages.getString("other")+" :");
		jLabel14.setText(messages.getString("employee_id")+":");
		jLabel10.setText(messages.getString("total_amount")+" :");
		jLabel16.setText(messages.getString("logged_in_as")+" :");
		lbl_total.setText("0.00");

		txt_emp.setText("emp");
		txt_bonus.setText("0");
		txt_other.setText("0");
		txt_hw.setText("0");
		txt_per.setText("0");
		txt_total_overtime.setText("0");
		txt_med.setText("0");
		
//		Fonts:
		jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
		lbl_total.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
		
		
//		Action Listener
		
		
		

	
		jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, messages.getString("search"),
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Adobe Arabic", 1, 14))); // NOI18N

		
		txt_search.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				txt_searchKeyReleased(evt);
			}
		});

		lbl_total.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				lbl_totalKeyTyped(evt);
			}
		});

		

		txt_cal.setIcon(
				new javax.swing.ImageIcon(getClass().getResource("/Images/Calculate.png"))); // NOI18N
		txt_cal.setText(messages.getString("calculate"));
		txt_cal.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				txt_calActionPerformed(evt);
			}
		});

		jButton1.setIcon(
				new javax.swing.ImageIcon(getClass().getResource("/Images/erase-128.png"))); // NOI18N
		jButton1.setText(messages.getString("clear"));
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});
		
		//GroupLayout End

		table_allowance
				.setModel(new javax.swing.table.DefaultTableModel(
						new Object[][] { { null, null, null, null }, { null, null, null, null },
								{ null, null, null, null }, { null, null, null, null } },
						new String[] { "Title 1", "Title 2", "Title 3", "Title 4" }));
		jScrollPane1.setViewportView(table_allowance);

		


		setJMenuBar(jMenuBar1);
		pack();
	}

	// </editor-fold>//GEN-END:initComponents




	private void txt_searchKeyReleased(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txt_searchKeyReleased
		// TODO add your handling code here:
		try {

			String sql = "select * from Staff_information where id=? ";

			pst = conn.prepareStatement(sql);
			pst.setString(1, txt_search.getText());
			rs = pst.executeQuery();

			String add1 = rs.getString("id");
			txt_empid.setText(add1);

			String add2 = rs.getString("first_name");
			txt_firstname.setText(add2);

			String add3 = rs.getString("surname");
			txt_surname.setText(add3);

			String add4 = rs.getString("Dob");
			txt_dob.setText(add4);

			String add5 = rs.getString("Salary");
			txt_salary.setText(add5);

			String add6 = rs.getString("Department");
			txt_dept.setText(add6);

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {

			try {

				rs.close();
				pst.close();

			} catch (Exception e) {

			}
		}
	}

	// GEN-LAST:event_txt_searchKeyReleased


	private void txt_calActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_txt_calActionPerformed
		// TODO add your handling code here:

		int salary = Integer.parseInt(txt_salary.getText());
		int overtime = Integer.parseInt(txt_hw.getText());

		double eight = 8;
		double days = 25;
		double dbop = 0;
		double overtimeRate = 1.5;

		// calculate the total hours of overtime
		double Total_Overtime = overtime * overtimeRate;
		String x = String.valueOf(Total_Overtime);
		txt_total_overtime.setText(x);

		// calculate overall overtime
		dbop = salary / days / eight;
		String s = String.valueOf(dbop);
		txt_per.setText(s);

		int med = Integer.parseInt(txt_med.getText());
		int bonus = Integer.parseInt(txt_bonus.getText());
		int other = Integer.parseInt(txt_other.getText());
		int f = med + bonus + other;
		double calc = Total_Overtime * dbop + f;
		String c = String.valueOf(calc);
		lbl_total.setText(c);

	}

	// GEN-LAST:event_txt_calActionPerformed

	private void lbl_totalKeyTyped(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_lbl_totalKeyTyped
		// TODO add your handling code here:

	}

	// GEN-LAST:event_lbl_totalKeyTyped

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		// TODO add your handling code here:

		//reset Text to default:
		txt_empid.setText("");
		txt_firstname.setText("");
		txt_surname.setText("");
		txt_salary.setText("");
		txt_dob.setText("");
		txt_dept.setText("");
		txt_med.setText("0");
		txt_bonus.setText("0");
		txt_other.setText("0");
		txt_hw.setText("0");
		txt_per.setText("0");
		txt_total_overtime.setText("0");
		lbl_total.setText("0.00");

	}

	// GEN-LAST:event_jButton1ActionPerformed

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton2ActionPerformed
		// TODO add your handling code here:
		if (txt_empid.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_employee_field"));
		} else if (txt_firstname.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_firstname_field"));

		} else if (txt_surname.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_surname_field"));

		} else if (txt_dob.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_dob_field"));

		} else if (txt_salary.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_salary_field"));

		} else if (txt_dept.getText().equals("")) {
			JOptionPane.showMessageDialog(null, messages.getString("empty_dep_field"));

		}

		else {

			int p = JOptionPane.showConfirmDialog(null, messages.getString("confirm_save_record"), messages.getString("add_record"),
					JOptionPane.YES_NO_OPTION);
			if (p == 0) {
				try {

					int value = Emp.empId;
					String value1 = txt_salary.getText();
					String value2 = txt_bonus.getText();
					String value3 = txt_med.getText();
					String value4 = txt_other.getText();
					String value5 = txt_per.getText();
					String value6 = txt_hw.getText();
					String value7 = lbl_total.getText();
					String value8 = txt_empid.getText();
					String value9 = txt_firstname.getText();
					String value10 = txt_surname.getText();

					String sql = "insert into Allowance (created_by,emp_id,overtime,medical,bonus,other,salary,rate,total_allowance,firstname,surname) values ('"
							+ value + "','" + value8 + "','" + value6 + "','" + value3 + "','" + value2 + "','" + value4
							+ "','" + value1 + "','" + value5 + "','" + value7 + "','" + value9 + "','" + value10
							+ "')";

					pst = conn.prepareStatement(sql);
					pst.execute();
					JOptionPane.showMessageDialog(null, messages.getString("allowance_added"));

				} catch (Exception e)

				{
					JOptionPane.showMessageDialog(null, e);
				}
				try {
					
					Localization _loc = new Localization();
					int val = Emp.empId;
					String reg = "insert into Audit (emp_id, date, status) values ('" + val + "','" + _loc.getTimeString() + " / "
							+ _loc.getDateString() + "','Updated Allowance Record')";
					pst = conn.prepareStatement(reg);
					pst.execute();
				} catch (Exception e)

				{
					JOptionPane.showMessageDialog(null, e);
				} finally {

					try {
						rs.close();
						pst.close();

					} catch (Exception e) {

					}
				}
			}
		}
		Update_table();
	}

	// GEN-LAST:event_jButton2ActionPerformed


	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {

		
		try {

			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Allowance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Allowance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Allowance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Allowance.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		// </editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Allowance().setVisible(true);
			}
		});
	}

	

	// Variables declaration
	private javax.swing.JButton jButton1;

	
	private javax.swing.JButton jButton2;

	
	private javax.swing.JLabel jLabel1;

	
	private javax.swing.JLabel jLabel10;

	
	private javax.swing.JLabel jLabel11;

	
	private javax.swing.JLabel jLabel12;

	
	private javax.swing.JLabel jLabel13;

	
	private javax.swing.JLabel jLabel14;

	
	private javax.swing.JLabel jLabel15;

	
	private javax.swing.JLabel jLabel16;

	
	private javax.swing.JLabel jLabel2;

	
	private javax.swing.JLabel jLabel3;

	
	private javax.swing.JLabel jLabel4;

	
	private javax.swing.JLabel jLabel5;

	
	private javax.swing.JLabel jLabel6;

	
	private javax.swing.JLabel jLabel7;

	
	private javax.swing.JLabel jLabel8;

	
	private javax.swing.JLabel jLabel9;

	
	private javax.swing.JMenuBar jMenuBar1;

	
	private javax.swing.JPanel jPanel1;

	
	private javax.swing.JPanel jPanel2;

	
	private javax.swing.JPanel jPanel4;

	
	private javax.swing.JScrollPane jScrollPane1;

	
	private javax.swing.JLabel lbl_total;

	
	private javax.swing.JTable table_allowance;

	
	private javax.swing.JTextField txt_bonus;

	
	private javax.swing.JButton txt_cal;

	
	private javax.swing.JTextField txt_dept;

	
	private javax.swing.JTextField txt_dob;

	
	private javax.swing.JLabel txt_emp;

	
	private javax.swing.JTextField txt_empid;

	
	private javax.swing.JTextField txt_firstname;

	
	private javax.swing.JTextField txt_hw;

	
	private javax.swing.JTextField txt_med;

	
	private javax.swing.JTextField txt_other;

	
	private javax.swing.JTextField txt_per;

	
	private javax.swing.JTextField txt_salary;

	
	private javax.swing.JTextField txt_search;

	
	private javax.swing.JTextField txt_surname;

	
	private javax.swing.JTextField txt_total_overtime;


}
