package de.ovgu.isp.ems.logging;

import org.apache.logging.log4j.Logger;


public aspect Logging {
	
	Logger logger = new ApplicationLogger().getApplicationLogger();
	
	
	after(): execution(String login.initPwdfield()) {
		logger.info("password field successfully initialized");
	}
	after(): execution(String login.initUserfield()) {
		logger.info("username field  successfully initialized");
	}
	after(): execution(void login.initComponents()) {
		logger.info("login UI components successfully initialized");
	}
	after(): execution(void login.loginPerformed(String, String)) {
		logger.info("login successfully performed");
	}
	
	
	
	
	

}
