package de.ovgu.isp.ems.core;

import java.util.Locale;
import java.util.ResourceBundle;

public aspect LDE {
	
	/**
	 * 
	 * @return german localization ResourceBundle
	 */
	ResourceBundle around() : execution(ResourceBundle Localization.getBundle()){
		Locale loc = new Locale("de", "DE");
		return ResourceBundle.getBundle("MessagesBundle", loc);
	}

}
