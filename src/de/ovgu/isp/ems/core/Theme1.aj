package de.ovgu.isp.ems.core;

import java.awt.Color;
import java.awt.Font;

public aspect Theme1 {
	
	Color around() : execution(Color Theme.getColor()){
		Color _color = Color.BLACK;
		return _color;
	}
	
	Font around() : execution(Font Theme.getFont()) {
		Font _font = new Font("TimesNewRoman", Font.BOLD, 12);
		return _font;
	}
	
	String around() : execution(String Theme.getName()){
		String _name = "TimesNewRoman";
		return _name;
	}
	
	int around() : execution(int Theme.getSize()){
		int _size = 14;
		return _size;
	}

	String around() : execution(String Theme.getThemeBackground()){
		String _imageLocation = "/Images/bk2.jpg";
		return _imageLocation;

	}

}
