package de.ovgu.isp.ems.core;

import java.util.Locale;
import java.util.ResourceBundle;

public aspect LUS {

	/**
	 * 
	 * @return US localization ResourceBundle with english language
	 */
	ResourceBundle around() : execution(ResourceBundle Localization.getBundle()){
		Locale loc = new Locale("en", "US");
		return ResourceBundle.getBundle("MessagesBundle", loc);
	}

}
