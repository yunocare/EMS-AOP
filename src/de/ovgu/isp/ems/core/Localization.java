package de.ovgu.isp.ems.core; 

import java.text.DateFormat; 
import java.text.SimpleDateFormat; 
import java.util.Date; 
import java.util.GregorianCalendar; 
import java.util.Locale; 
import java.util.ResourceBundle; 


import java.util.Calendar; 

public   class  Localization {
	
	// mandatory feature:

	public Localization() {

	}

	

	public String getTimeString() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(d);

		return timeString;

	}

	

	public String getDateString() {
		Date currentDate = GregorianCalendar.getInstance().getTime();
		DateFormat df = DateFormat.getDateInstance();
		String dateString = df.format(currentDate);
		return dateString;

	}

	

	/**
	 * 
	 * @return Resource Bundle in current configuration
	 */
	public ResourceBundle getBundle() {
		return ResourceBundle.getBundle("MessagesBundle", getCurrentLocale());
	}

	

	public String getCountrySpecificDate() {
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return " | " + year + "-" + (month + 1) + "-" + day + " | ";
	}

	

	// get locale united states
	public Locale getCurrentLocale() {
		Locale _locale = new Locale("en", "US");
		return _locale;
	}


}
