package de.ovgu.isp.ems.core;

import java.awt.Color;
import java.awt.Font;

public class Theme {

	public Color getColor() {
		Color _color = Color.BLACK;
		return _color;
	}

	public Font getFont() {
		Font _font = new Font("TimesNewRoman", Font.BOLD, 12);
		return _font;
	}

	public String getName() {
		String _name = "TimesNewRoman";
		return _name;
	}

	// return Theme Font Size:
	public int getSize() {
		int _size = 14;
		return _size;
	}

	public String getThemeBackground() {
		String _imageLocation = "/Images/bk2.jpg";
		return _imageLocation;
	}

}
