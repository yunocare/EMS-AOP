package de.ovgu.isp.ems.db; 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File; 
import java.sql.Connection; 
import javax.swing.JOptionPane; 
import java.sql.DriverManager; 

/**
 * Setting up connection to SQLite Database
 * 
 * @author fs
 *
 */
public  class  db {
	

	static Connection conn = null;

	

	// returns the DB connection
	public static Connection java_db() {
		try {
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager.getConnection("jdbc:sqlite:." + File.separator + "src" + File.separator + "empnet.sqlite");
//			JOptionPane.showMessageDialog(null, "Connection to database is successful");
			return conn;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
			return null;
		}

	}


}
