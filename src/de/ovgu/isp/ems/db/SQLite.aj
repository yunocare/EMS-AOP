package de.ovgu.isp.ems.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public aspect SQLite {
	
	@SuppressWarnings("finally")
	Connection around(): execution(Connection db.java_db()){
		Connection conn = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:." + File.separator + "src" + File.separator + "empnet.sqlite");
			JOptionPane.showMessageDialog(null, "Connection to database is successful");
		} catch (ClassNotFoundException e ) {
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		} catch(SQLException e){
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		}finally{
			return conn;
		}
	}

}
