<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	<featureModel>
		<properties/>
		<struct>
			<and mandatory="true" name="EMS">
				<description>
					An Employee Management System (EMS) is modelled.
					The Applications should provide a software solution 
					that can be used to manage Employees, update 
					Salaries and generate reports on current data.
				</description>
				<and mandatory="true" name="Database">
					<description>
						The "Employee Management System" Application 
						stores data on users, employees and 
						departments, a Database is required in EMS.
					</description>
					<feature mandatory="true" name="SQLite">
						<description>
							SQLite Database as general database solution.
						</description>
					</feature>
				</and>
				<and abstract="true" mandatory="true" name="MessageBundle">
					<description>
						A MessageBundle is needed to display content in a 
						chosen language. The MessageBundle will be 
						translated for German, English and Russian as well.
					</description>
					<alt mandatory="true" name="Localization">
						<description>
							Country selection
						</description>
						<feature name="LUS">
							<description>
								Localization for United States (US)
							</description>
						</feature>
						<feature name="LDE">
							<description>
								Localization Germany (DE)
							</description>
						</feature>
						<feature name="LRU">
							<description>
								Localization Russia (RU)
							</description>
						</feature>
					</alt>
					<alt abstract="true" mandatory="true" name="Internationalization">
						<description>
							language selection
						</description>
						<feature name="en">
							<description>
								display content in english language (en)
							</description>
						</feature>
						<feature name="de">
							<description>
								display content in german language (de)
							</description>
						</feature>
						<feature name="ru">
							<description>
								display content in russian language (ru)
							</description>
						</feature>
					</alt>
				</and>
				<alt mandatory="true" name="MainMenuUI">
					<feature name="ButtonMenuUI"/>
					<feature name="ListMenuUI"/>
				</alt>
				<and abstract="true" mandatory="true" name="LayoutFont">
					<description>
						used font on custom layout
					</description>
					<alt abstract="true" mandatory="true" name="Color">
						<description>
							font color on custom layout
						</description>
						<feature name="Black"/>
						<feature name="Red"/>
						<feature name="Blue"/>
					</alt>
					<alt abstract="true" mandatory="true" name="Font">
						<description>
							font type
						</description>
						<feature name="Arial"/>
						<feature name="TimesNewRoman"/>
						<feature name="Italic"/>
					</alt>
					<alt abstract="true" mandatory="true" name="FontSize">
						<description>
							font size customizable
						</description>
						<feature name="S11"/>
						<feature name="S12"/>
						<feature name="S14"/>
					</alt>
				</and>
				<feature mandatory="true" name="Base">
					<description>
						The base feature covers the application's core 
						functionalities.
					</description>
				</feature>
				<alt mandatory="true" name="AccessController">
					<description>
						AccessController feature is required in a EMS 
						product line to manage access to the portal. It covers
						user creation and maintain user accounts.
					</description>
					<feature name="TwoFA">
						<description>
							2FA: two factor authentication.
							Authetication requires username and password.
						</description>
					</feature>
					<feature name="ThreeFA">
						<description>
							3FA: three factor authentication.
							Authetication requires username, password 
							as well as a Role selection (e.g. Role "Admin", "Sales", 
							"HR")
						</description>
					</feature>
				</alt>
				<and name="ReportGenerator">
					<description>
						Custom reports on Employee selection and 
						accounting data can be  generated, while SQL 
						queries are executed.
					</description>
					<feature name="PDFGenerator">
						<description>
							A PDF Generator export database requests to .pdf 
							format and generates a new file.
						</description>
					</feature>
					<feature name="XMLGenerator">
						<description>
							An XML Generator export database requests to .xml 
							format and generates a new file.
						</description>
					</feature>
					<feature name="CSVGenerator">
						<description>
							A CSV Generator export database requests to .csv 
							format and generates a new file.
						</description>
					</feature>
				</and>
				<alt abstract="true" name="LayoutTheme">
					<description>
						A layout them can be chosen to customize  graphical
						appilaction components. (e.g. Background-Picture,
						Fonts and Sizes)
					</description>
					<feature name="Theme1">
						<description>
							dark theme
						</description>
					</feature>
					<feature name="Theme2">
						<description>
							blue custom theme
						</description>
					</feature>
					<feature name="Theme3">
						<description>
							fancy custom theme
						</description>
					</feature>
				</alt>
				<feature name="ApplicationLogging">
					<description>
						If this feature is selected, A Logger will log
						performed actions, in conseuqence a .log file 
						will be generated.
					</description>
				</feature>
				<feature name="EmployeeSearch">
					<description>
						This features provides functionalities on searching
						the database for employees. Employee related 
						information is displayed on a seperate Window.
					</description>
				</feature>
				<feature name="OvertimeCalculator">
					<description>
						Overtime calculation can be selected, so a 
						functionality to calculate overtime hours is enabled.
						An application admin user can see the working hours
						of each employee.
					</description>
				</feature>
				<feature name="SalaryUpdater">
					<description>
						The SalaryUpdater provides functionality to update 
						the salary of a chosen employee by a specific 
						percentage or fixed amount.
					</description>
				</feature>
				<feature name="AuditTrail">
					<description>
						The AuditTrail can be selected to log user login
						sessions, performed actions and updates that have 
						been made.
					</description>
				</feature>
				<feature name="EmployeeAdder"/>
			</and>
		</struct>
		<constraints>
			<rule>
				<imp>
					<var>Theme1</var>
					<conj>
						<var>Black</var>
						<conj>
							<var>Arial</var>
							<conj>
								<var>S12</var>
								<var>ButtonMenuUI</var>
							</conj>
						</conj>
					</conj>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>Theme2</var>
					<conj>
						<var>Blue</var>
						<conj>
							<var>TimesNewRoman</var>
							<disj>
								<var>S11</var>
								<var>S14</var>
							</disj>
						</conj>
					</conj>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>Theme3</var>
					<conj>
						<var>Red</var>
						<conj>
							<var>Italic</var>
							<conj>
								<var>S11</var>
								<var>ListMenuUI</var>
							</conj>
						</conj>
					</conj>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>LUS</var>
					<var>en</var>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>LDE</var>
					<var>de</var>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>LRU</var>
					<var>ru</var>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>OvertimeCalculator</var>
					<var>EmployeeSearch</var>
				</imp>
			</rule>
			<rule>
				<imp>
					<var>SalaryUpdater</var>
					<var>EmployeeSearch</var>
				</imp>
			</rule>
		</constraints>
		<calculations Auto="true" Constraints="true" Features="true" Redundant="true" Tautology="true"/>
		<comments/>
		<featureOrder userDefined="false"/>
	</featureModel>
